// Этот реализация паттерна Proxy в качестве логгера

import { OutputApi } from './OutputApi.mjs';
import { Logger } from '../utils/Logger.mjs';

export class OutputApiProxy extends OutputApi {
	say(message) {
		Logger.log(message);
		super.say(message);
	}
}
