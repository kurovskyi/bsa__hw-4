export class OutputApi {
	constructor({ onMessageHandler }) {
		this.onMessageHandler = onMessageHandler;
	}

	say(message) {
		this.onMessageHandler(message);
	}
}
