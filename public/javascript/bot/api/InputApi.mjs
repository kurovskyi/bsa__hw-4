export class InputApi {
	constructor({
		controller,
		onGameEnd,
		getRacerNames,
		getWonRacerNames,
		getRacerProgresses,
	}) {
		this.controller = controller;

		this.onGameEnd = onGameEnd;
		this.getRacerNames = getRacerNames;
		this.getWonRacerNames = getWonRacerNames;
		this.getRacerProgresses = getRacerProgresses;
	}
}
