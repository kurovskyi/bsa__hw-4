// Этот реализация паттерна Фасад, данный класс выступает в роли API-интерфейса бота

import { InputApi } from './api/InputApi.mjs';
import { OutputApiProxy } from './api/OutputApiProxy.mjs';
import { MessageService } from './services/message/MessageService.mjs';
import { JokeServiceFactory } from './services/joke/JokeServiceFactory.mjs';
import { Controller } from './controller/Controller.mjs';

export class Bot {
	constructor({ onMessageHandler, ...inputApiMethods }) {
		const inputApi = new InputApi({ ...inputApiMethods });
		const outputApi = new OutputApiProxy({ onMessageHandler });
		const messageService = new MessageService();
		const jokeServiceFactory = new JokeServiceFactory();
		const jokeLocalService = jokeServiceFactory.create('funny');
		this.controller = new Controller({
			inputApi,
			outputApi,
			messageService,
			jokeService: jokeLocalService,
		});
	}

	triggerStart() {
		this.controller.startProcess();
	}

	triggerRace() {
		this.controller.raceProcess();
	}

	triggerEnd() {
		this.controller.endProcess();
	}
}
