// Этот реализация паттерна Factory

import { JokeFunnyService } from './JokeFunnyService.mjs';

export class JokeServiceFactory {
	create(type = 'funny') {
		switch (type) {
			// Подойдет если у нас предполагается несколько способов получить шуточки
			// case 'notFunny':
			// 	return new JokeNotFunnyService();
			default:
				return new JokeFunnyService();
		}
	}
}
