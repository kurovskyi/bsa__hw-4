import { FUNNY_JOKES } from './jokes.mjs';

export class JokeFunnyService {
	getRandomJoke() {
		return FUNNY_JOKES[Math.floor(Math.random() * FUNNY_JOKES.length)];
	}
}
