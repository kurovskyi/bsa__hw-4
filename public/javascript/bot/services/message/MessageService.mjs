export class MessageService {
	getStartMessage(racerNames) {
		return `Скоро будет начало игры! Наши великие участники гонки: \n${racerNames.join(
			', '
		)}`;
	}

	getPreRaceMessage() {
		return 'Ну что, погнали!!!';
	}

	getRaceMessage(racers) {
		return `А гонка идёт... Имеем сейчас следующие результаты: \n${[...racers]
			.sort(({ progress: a }, { progress: b }) => b - a)
			.map(
				({ name, progress }, i) =>
					`${i + 1}. ${name} с преодолевшей дистанцией ${progress}%.`
			)
			.join('\n')}`;
	}

	getJokeMessage(joke) {
		return `Давайте отвлечемся на смешную шутку: ${joke}`;
	}

	getEndMessage(racerNames) {
		return `Конец гонки! Результаты: \n${
			racerNames?.length
				? racerNames.map((name, i) => `${i + 1}. ${name}`).join('\n')
				: 'Никто не выиграл!'
		}`;
	}
}
