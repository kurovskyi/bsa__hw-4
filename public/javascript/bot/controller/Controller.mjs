export class Controller {
	state = {
		raceInterval: null,
	};

	constructor({ inputApi, outputApi, messageService, jokeService }) {
		this.inputApi = inputApi;
		this.outputApi = outputApi;
		this.messageService = messageService;
		this.jokeService = jokeService;
	}

	setState(newState) {
		this.state = Object.assign({}, newState);
	}

	startProcess() {
		const racerNames = this.inputApi.getRacerNames();

		const startMsg = this.messageService.getStartMessage(racerNames);
		this.outputApi.say(startMsg);
	}

	// [racers: {name, progress}]
	raceProcess() {
		const preRaceMsg = this.messageService.getPreRaceMessage();
		this.outputApi.say(preRaceMsg);

		const interval = setInterval(() => {
			const { racers, secondsLeft } = this.inputApi.getRacerProgresses();

			const raceMsg = this.messageService.getRaceMessage(racers);
			this.outputApi.say(raceMsg);

			setTimeout(() => {
				if (!this.state.raceInterval) {
					return;
				}

				const jokeMsg = this.messageService.getJokeMessage(
					this.jokeService.getRandomJoke()
				);
				this.outputApi.say(jokeMsg);
			}, 15000);
		}, 30000);

		this.setState({ raceInterval: interval });
	}

	endProcess() {
		const racerNames = this.inputApi.getWonRacerNames();

		const endMsg = this.messageService.getEndMessage(racerNames);
		this.outputApi.say(endMsg);

		clearInterval(this.state.raceInterval);
		this.setState({ raceInterval: null });

		this.inputApi.onGameEnd();
	}
}
