import {
	MAXIMUM_USERS_FOR_ONE_ROOM,
	SECONDS_TIMER_BEFORE_START_GAME,
	SECONDS_FOR_GAME,
} from './config';

import {
	getAllOtherConnectedSockets,
	getAllRooms,
	checkRoomExists,
	getRoom,
	getRoomUsersCount,
	checkUsersAreReady,
	setSocketReady,
	setSocketNotReady,
	getRandomTextIndex,
	checkAllSocketsWon,
	getAllSocketsWon,
	getAllConnectedSockets,
} from './helpers';

export function handleRoomsLogic(io, socket) {
	const username = socket.handshake.query.username;

	const allConnectedSockets = getAllOtherConnectedSockets(io, socket);
	const isUsedUsername = !!allConnectedSockets.find(
		(otherSocket) => otherSocket.handshake.query.username === username
	);

	if (isUsedUsername) {
		socket.emit('USERNAME_ALREADY_USED_ERROR');
	}

	socket.emit('ROOMS_DATA', getAllRooms(io));

	socket.on('CREATE_ROOM_TRIGGER', (roomName) => {
		if (checkRoomExists(io, roomName)) {
			return socket.emit('ROOM_ALREADY_EXISTS_ERROR');
		}

		socket.join(roomName);

		io.sockets.adapter.rooms[roomName].isGameProcessStarted = false;
		io.sockets.adapter.rooms[roomName].gamesPlayedCount = 0;
		io.sockets.adapter.rooms[roomName].readySockets = [];
		io.sockets.adapter.rooms[roomName].wonSockets = [];

		socket.emit('JOINED_ROOM_TRIGGER');
		socket.emit('ROOM_DATA', getRoom(io, roomName));
		socket.broadcast.emit('ROOMS_DATA', getAllRooms(io));
	});

	socket.on('JOIN_ROOM_TRIGGER', (roomName) => {
		socket.join(roomName);

		if (getRoomUsersCount(io, roomName) >= MAXIMUM_USERS_FOR_ONE_ROOM) {
			return socket.emit('JOIN_ROOM_MAX_USERS_REACHED_ERROR');
		}

		socket.emit('JOINED_ROOM_TRIGGER');
		socket.emit('ROOM_DATA', getRoom(io, roomName));
		socket.broadcast.to(roomName).emit('ROOM_DATA', getRoom(io, roomName));
		socket.broadcast.emit('ROOMS_DATA', getAllRooms(io));
	});

	function startGameProcess(io, socket, roomName) {
		if (checkUsersAreReady(io, roomName)) {
			io.sockets
				.in(roomName)
				.emit('ROOM_TIMER_TRIGGER', SECONDS_TIMER_BEFORE_START_GAME);
			io.sockets.adapter.rooms[roomName].isGameProcessStarted = true;
			io.sockets.emit('ROOMS_DATA', getAllRooms(io));

			let timer = SECONDS_TIMER_BEFORE_START_GAME;
			const interval = setInterval(() => {
				if (timer === 0) {
					clearInterval(interval);

					const gamesCount =
						io.sockets.adapter.rooms[roomName].gamesPlayedCount;
					let gameTimer = SECONDS_FOR_GAME;
					const gameInterval = setInterval(() => {
						if (gameTimer === 0) {
							clearInterval(gameInterval);
							if (
								gamesCount ===
									io.sockets.adapter.rooms[roomName].gamesPlayedCount &&
								io.sockets.adapter.rooms[roomName].isGameProcessStarted &&
								!checkAllSocketsWon(io, socket, roomName)
							) {
								io.sockets.in(roomName).emit(
									'END_GAME_TRIGGER',
									getAllSocketsWon(io, socket, roomName).map(
										(socket) =>
											getAllConnectedSockets(io)[socket].handshake.query
												.username
									)
								);

								io.sockets.adapter.rooms[roomName].isGameProcessStarted = false;
								io.sockets.adapter.rooms[roomName].gamesPlayedCount += 1;
								io.sockets.adapter.rooms[roomName].readySockets = [];
								io.sockets.adapter.rooms[roomName].wonSockets = [];

								io.sockets
									.in(roomName)
									.emit('ROOM_DATA', getRoom(io, roomName));
								io.sockets.emit('ROOMS_DATA', getAllRooms(io));

								return;
							}
						}
						gameTimer -= 1;
					}, 1000);

					return io.sockets.in(roomName).emit('START_GAME_TRIGGER', {
						textIndex: getRandomTextIndex(),
						timer: SECONDS_FOR_GAME,
					});
				}
				timer -= 1;
			}, 1000);
		}
	}

	socket.on('LEAVE_ROOM_TRIGGER', (roomName) => {
		socket.leave(roomName);
		socket.emit('LEAVED_ROOM_TRIGGER');
		if (checkRoomExists(io, roomName)) {
			socket.broadcast.emit('ROOM_DATA', getRoom(io, roomName));
			startGameProcess(io, socket, roomName);
		}
		socket.emit('ROOMS_DATA', getAllRooms(io));
		socket.broadcast.emit('ROOMS_DATA', getAllRooms(io));
	});

	socket.on('READY_TRIGGER', (roomName) => {
		setSocketReady(io, socket, roomName);
		io.sockets.in(roomName).emit('ROOM_DATA', getRoom(io, roomName));
		startGameProcess(io, socket, roomName);
	});

	socket.on('NOT_READY_TRIGGER', (roomName) => {
		setSocketNotReady(io, socket, roomName);
		io.sockets.in(roomName).emit('ROOM_DATA', getRoom(io, roomName));
	});

	// socket.on('disconnection', () => {
	// 	Object.keys(socket.rooms).forEach((roomName) => {
	// 		if (roomName !== socket.id) {
	// 			socket.leave(roomName);
	// 			io.sockets.in(roomName).emit('ROOM_DATA', getRoom(io, roomName));
	// 			if (checkUsersAreReady(io, roomName)) {
	// 				io.sockets
	// 					.in(roomName)
	// 					.emit('ROOM_TIMER_TRIGGER', SECONDS_TIMER_BEFORE_START_GAME);
	// 			}
	// 		}
	// 	});
	// });

	socket.on('disconnect', () => {
		socket.broadcast.emit('ROOMS_DATA', getAllRooms(io));
	});
}
