import { texts } from '../data';

export function getAllConnectedSockets(io) {
	return io.sockets.sockets;
}

export function getAllOtherConnectedSockets(io, socket) {
	return Object.values(
		Object.fromEntries(
			Object.entries(getAllConnectedSockets(io)).filter(
				([otherSocketId]) => otherSocketId !== socket.id
			)
		)
	);
}

export function getAllRooms(io) {
	const allRooms = io.sockets.adapter.rooms;
	const parsedRooms = [];
	if (allRooms) {
		for (const roomKey in allRooms) {
			if (
				!allRooms[roomKey].sockets.hasOwnProperty(roomKey) &&
				!allRooms[roomKey].isGameProcessStarted
			) {
				parsedRooms.push({
					name: roomKey,
					usersCount: allRooms[roomKey].length,
				});
			}
		}
	}
	return parsedRooms;
}

export function checkRoomExists(io, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	return allRooms.hasOwnProperty(roomName);
}

export function getRoomUsersCount(io, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	return allRooms[roomName].length;
}

export function getRoom(io, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	const roomSocketIds = allRooms[roomName].sockets;
	const roomReadySocketIds = allRooms[roomName].readySockets;
	return {
		name: roomName,
		users: Object.keys(roomSocketIds).map((socketId) => ({
			name: getAllConnectedSockets(io)[socketId].handshake.query.username,
			isReady: roomReadySocketIds?.includes(socketId) ?? false,
		})),
	};
}

export function setSocketReady(io, socket, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	const roomReadySocketIds = allRooms[roomName].readySockets;
	roomReadySocketIds.push(socket.id);
}

export function setSocketNotReady(io, socket, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	const roomReadySocketIds = allRooms[roomName].readySockets;
	const socketIndex = roomReadySocketIds.indexOf(socket.id);
	if (socketIndex > -1) {
		roomReadySocketIds.splice(socketIndex, 1);
	}
}

export function checkUsersAreReady(io, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	const roomSocketIds = allRooms[roomName].sockets;
	const roomReadySocketIds = allRooms[roomName].readySockets;
	return Object.keys(roomSocketIds).length === roomReadySocketIds.length;
}

export function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getRandomTextIndex() {
	const randomTextIndex = getRandomInt(0, texts.length - 1);
	return randomTextIndex;
}

export function setSocketWon(io, socket, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	const roomWonSocketIds = allRooms[roomName].wonSockets;
	roomWonSocketIds.push(socket.id);
}

export function checkAllSocketsWon(io, socket, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	const roomSocketIds = allRooms[roomName].sockets;
	const roomWonSocketIds = allRooms[roomName].wonSockets;
	return Object.keys(roomSocketIds).length === roomWonSocketIds.length;
}

export function getAllSocketsWon(io, socket, roomName) {
	const allRooms = io.sockets.adapter.rooms;
	return allRooms[roomName].wonSockets;
}
