import {
	setSocketWon,
	checkAllSocketsWon,
	getAllSocketsWon,
	getAllConnectedSockets,
	getAllRooms,
	getRoom,
} from './helpers';

export function handleGameLogic(io, socket) {
	const username = socket.handshake.query.username;

	socket.on('PROGRESS_TRIGGER', ({ roomName, progress }) => {
		socket.broadcast
			.to(roomName)
			.emit('PROGRESS_TRIGGER', { socketUsername: username, progress });

		if (progress === 100) {
			setSocketWon(io, socket, roomName);

			if (checkAllSocketsWon(io, socket, roomName)) {
				io.sockets.in(roomName).emit(
					'END_GAME_TRIGGER',
					getAllSocketsWon(io, socket, roomName).map(
						(socket) =>
							getAllConnectedSockets(io)[socket].handshake.query.username
					)
				);

				io.sockets.adapter.rooms[roomName].isGameProcessStarted = false;
				io.sockets.adapter.rooms[roomName].gamesPlayedCount += 1;
				io.sockets.adapter.rooms[roomName].readySockets = [];
				io.sockets.adapter.rooms[roomName].wonSockets = [];

				io.sockets.in(roomName).emit('ROOM_DATA', getRoom(io, roomName));
				io.sockets.emit('ROOMS_DATA', getAllRooms(io));
			}
		}
	});
}
